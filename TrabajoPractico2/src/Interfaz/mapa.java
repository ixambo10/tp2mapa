package Interfaz;
import java.awt.EventQueue;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import Grafos.ArbolGeneradorMinimo;
import Grafos.Coordenadas;
import Grafos.Grafo;
import javax.swing.JTextField;
import java.awt.Font;


import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class mapa extends JFrame {

	private JPanel contentPane;
	private JMapViewer _mapa;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtCantidad;
	private ArrayList<Coordinate> _lasCoordenadas;
	private MapPolygonImpl _poligono;
	private double[][] matrizAGM;
	private boolean antesAGM=false;
	private ArrayList<Coordenadas> listaCoordenadas = new ArrayList<Coordenadas>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mapa frame = new mapa();
					frame.setVisible(true);
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	// M�todo que genera los puntos de la instancia seleccionada en el mapa
	public void generadorDePuntos(String instancia) {
		listaCoordenadas.clear();
		int Punto = 1; // Identifica cada vertice con un Numero desde el 1 hacia adelante.
		
		try {
			FileInputStream fis = new FileInputStream(instancia);
			Scanner scanner = new Scanner(fis);

			while (scanner.hasNext()) {
				double valorX = Double.parseDouble(scanner.next());
				double valorY = Double.parseDouble(scanner.next());

				listaCoordenadas.add(new Coordenadas(valorX, valorY));

				_mapa.addMapMarker(new MapMarkerDot(String.valueOf(Punto), new Coordinate(valorX, valorY)));
				Punto = Punto + 1;
			}
			scanner.close();

		} catch (FileNotFoundException s) {
			s.printStackTrace();
			 System.err.println("ERROR\nNo se encuentra el archivo Instancia");
		}

	}

	// Metodo que realiza el Arbol generador Minimo
	private void algoritmoPrim(Grafo grafo) {
		ArbolGeneradorMinimo ArbolGM = new ArbolGeneradorMinimo(grafo);
		matrizAGM = ArbolGM.AlgPrim();
		representarAristasEnMapa(matrizAGM);
		antesAGM=true;
	}

	// Representaci�n de las aristas de la matriz en el mapa
	private void representarAristasEnMapa(double[][] A) {
		for (int i = 0; i < A.length; i++) {
			_lasCoordenadas = new ArrayList<Coordinate>();
			for (int j = 0; j < A[i].length; j++) {
				if (A[i][j] != 0) {
					Coordinate markeraddx = new Coordinate(listaCoordenadas.get(i).getx(),
							listaCoordenadas.get(i).gety());
					Coordinate markeraddy = new Coordinate(listaCoordenadas.get(j).getx(),
							listaCoordenadas.get(j).gety());
					_lasCoordenadas.add(markeraddx);
					_lasCoordenadas.add(markeraddy);
					_poligono = new MapPolygonImpl(_lasCoordenadas);
					_mapa.addMapPolygon(_poligono);
				}
			}
		}
	}

	// Metodo Generador de Grafo
	private Grafo generadorDeGrafo() {

		Grafo grafo = new Grafo(listaCoordenadas.size());

		for (int i = 0; i < listaCoordenadas.size() - 1; i++) {
			for (int j = i; j < listaCoordenadas.size(); j++) {
				if (i != j) {
					grafo.agregarArista(i, j, distanciaEntrePuntos(listaCoordenadas.get(i), listaCoordenadas.get(j)));
				}
			}
		}
		
		return grafo;
	}

	// Inicializa el mapa
	private void reiniciarMapa() {
		_mapa.removeAllMapPolygons();
		_mapa.removeAllMapMarkers();
	}

	// Cluster: Identifica las aristas con menor distancia y las inicializa en Cero. La cantidad es ingresada por el cliente
	private void divisionEnClusters(int cantidad) {
		
		if(antesAGM == true) {
				
		double[][] D = matrizAGM.clone();

		while (cantidad > 1) {
			double aux = -1;
			int verticeD = 0;
			int verticeA = 0;

			for (int i = 0; i < D.length; i++) {
				for (int j = 0; j < D[i].length; j++) {
					if (aux == -1) {
						aux = D[i][j];
					} else if (D[i][j] > aux) {
						aux = D[i][j];
						verticeD = i;
						verticeA = j;
						
					}
				}
			}
			D[verticeD][verticeA] = 0;
			D[verticeA][verticeD] = 0;
			cantidad = cantidad - 1;

			_mapa.removeAllMapPolygons();
		}
		antesAGM = false;
		representarAristasEnMapa(D);
		}
	}

	// Realiza el calculo de la distancia entre los puntos de entrada
	public double distanciaEntrePuntos(Coordenadas puntoA, Coordenadas puntoB) {

		double cateto1 = puntoA.getx() - puntoB.getx();
		double cateto2 = puntoA.gety() - puntoB.gety();
		double distancia = Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2));

		return distancia;
	}

	public mapa() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 628, 420);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel mapaVisible = new JPanel();
		mapaVisible.setBounds(10, 11, 422, 359);
		contentPane.add(mapaVisible);

		_mapa = new JMapViewer();   // Creaci�n del mapa
		mapaVisible.add(_mapa);

		JPanel comandos = new JPanel();
		comandos.setBounds(442, 11, 164, 359);
		contentPane.add(comandos);
		comandos.setLayout(null);

		JLabel lblSeleccioneInstancia = new JLabel("Seleccione Instancia");
		lblSeleccioneInstancia.setBounds(27, 11, 127, 23);
		comandos.add(lblSeleccioneInstancia);

		// Levanta de un txt las coordenadas de la instancia 1 y la muestra en el mapa
		JRadioButton rdbtnInstancia_1 = new JRadioButton("Instancia 1");
		rdbtnInstancia_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reiniciarMapa();   
				_mapa.setDisplayPosition(new Coordinate(-34.532, -58.71), 14); // Centra el Mapa en la coordenas de la Instancia 										
				generadorDePuntos("Instancia1.txt");
			}
		});
		buttonGroup.add(rdbtnInstancia_1);
		rdbtnInstancia_1.setBounds(10, 41, 109, 23);
		comandos.add(rdbtnInstancia_1);

		// Levanta de un txt las coordenadas de la instancia 2 y la muestra en el mapa
		JRadioButton rdbtnInstancia_2 = new JRadioButton("Instancia 2");
		rdbtnInstancia_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reiniciarMapa();
				_mapa.setDisplayPosition(new Coordinate(-34.529, -58.68), 12); // Centra el Mapa en la coordenas de la Instancia 																
				generadorDePuntos("Instancia2.txt");
			}
		});
		buttonGroup.add(rdbtnInstancia_2);
		rdbtnInstancia_2.setBounds(10, 69, 109, 23);
		comandos.add(rdbtnInstancia_2);

		// Levanta de un txt las coordenadas de la instancia 3 y la muestra en el mapa
		JRadioButton rdbtnInstancia_3 = new JRadioButton("Instancia 3");
		rdbtnInstancia_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reiniciarMapa();
				_mapa.setDisplayPosition(new Coordinate(-34.6637, -58.41), 12); // Centra el Mapa en la coordenas de la Instancia 		
				generadorDePuntos("Instancia3.txt");
			}
		});
		buttonGroup.add(rdbtnInstancia_3);
		rdbtnInstancia_3.setBounds(10, 95, 109, 23);
		comandos.add(rdbtnInstancia_3);

		// Levanta de un txt las coordenadas de la instancia 4 y la muestra en el mapa
		JRadioButton rdbtnInstancia_4 = new JRadioButton("Instancia 4");
		rdbtnInstancia_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reiniciarMapa();
				_mapa.setDisplayPosition(new Coordinate(-34.528, -58.52), 13); // Centra el Mapa en la coordenas de la Instancia 		
				generadorDePuntos("Instancia4.txt");
			}
		});
		buttonGroup.add(rdbtnInstancia_4);
		rdbtnInstancia_4.setBounds(10, 121, 109, 23);
		comandos.add(rdbtnInstancia_4);

		// Levanta de un txt las coordenadas de la instancia 5 y la muestra en el mapa
		JRadioButton rdbtnInstancia_5 = new JRadioButton("Instancia 5");
		rdbtnInstancia_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reiniciarMapa();
				_mapa.setDisplayPosition(new Coordinate(-34.5222, -58.700), 17); // Centra el Mapa en la coordenas de la Instancia 		
				generadorDePuntos("Instancia5.txt");
			}
		});
		buttonGroup.add(rdbtnInstancia_5);
		rdbtnInstancia_5.setBounds(10, 147, 109, 23);
		comandos.add(rdbtnInstancia_5);
		
		// Bot�n camino Minimo: Realiza un Arbol Generador Minimo entre los puntos en el mapa ( Se utiliza el Metodo de Prim )
		JButton btnAGM = new JButton("Camino M\u00EDnimo");
		btnAGM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Grafo grafo = generadorDeGrafo();
				algoritmoPrim(grafo);
			}
		});
		btnAGM.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAGM.setBounds(10, 188, 140, 40);
		comandos.add(btnAGM);
		
		//Toma el valor de las cantidad de Clusters que desea el usuario
		JLabel lblCantidadDeClusters = new JLabel("Cantidad de Clusters");
		lblCantidadDeClusters.setHorizontalAlignment(SwingConstants.CENTER);
		lblCantidadDeClusters.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCantidadDeClusters.setBounds(10, 234, 140, 27);
		comandos.add(lblCantidadDeClusters);

		txtCantidad = new JTextField();
		txtCantidad.setHorizontalAlignment(SwingConstants.CENTER);
		txtCantidad.setBounds(10, 261, 140, 23);
		comandos.add(txtCantidad);
		txtCantidad.setColumns(10);

		//Toma el valor de las cantidad de Clusters que desea el usuario
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	    
		            try {
		                divisionEnClusters(Integer.parseInt(txtCantidad.getText()));
		            } catch (NumberFormatException nfe) {
		                System.err.println("ERROR\nUsted debe ingresar n�meros");
		            }
   
				
				
			}
		});
		btnNewButton.setBounds(10, 306, 71, 42);
		comandos.add(btnNewButton);
		
		JButton btnNewButton_Salir = new JButton("Salir");
		btnNewButton_Salir.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnNewButton_Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnNewButton_Salir.setBounds(91, 308, 63, 40);
		comandos.add(btnNewButton_Salir);

	}
}
