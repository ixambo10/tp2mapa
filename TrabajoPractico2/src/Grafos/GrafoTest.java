package Grafos;


import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;


	public class GrafoTest 
	{
		@Test (expected=IllegalArgumentException.class)
		public void agregarAristaVerticeIgual()
		{
			Grafo g = new Grafo(5);
			g.agregarArista(0, 0, 0);
		}

		@Test (expected=IllegalArgumentException.class)
		public void agregarAristaFueraDeRangoSuperior()
		{
			Grafo g = new Grafo(5);
			g.agregarArista(5, 0, 0);
		}

		@Test (expected=IllegalArgumentException.class)
		public void agregarAristaFueraDeRangoSuperiorJ()
		{
			Grafo g = new Grafo(5);
			g.agregarArista(0, 5, 0);
		}
		
		@Test (expected=IllegalArgumentException.class)
		public void agregarAristaFueraDeRangoInferior()
		{
			Grafo g = new Grafo(5);
			g.agregarArista(-1, 0, 0);
		}

		@Test (expected=IllegalArgumentException.class)
		public void agregarAristaFueraDeRangoInferiorJ()
		{
			Grafo g = new Grafo(5);
			g.agregarArista(0, -1, 0);
		}
		
		@Test 
		public void valorArista()
		{
			Grafo g = new Grafo(5);
			g.agregarArista(0, 3, 6);
			assertEquals(6,(int)g.valorArista(0,3));
		}

		public void existeVertice()
		{
			Grafo g = new Grafo(3);
			ArrayList<Integer> array = new ArrayList<Integer>();
			array.get(1);
			array.get(2);
			array.get(3);
			assertEquals(g.ListaVertices(),array);
		}
		
		public void tama�o()
		{
			Grafo g = new Grafo(5);
			assertEquals(5,g.tamano());
		}
		
		
	
		

	}


