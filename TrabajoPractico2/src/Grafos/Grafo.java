package Grafos;


import java.util.HashSet;
import java.util.Set;

public class Grafo {
	
	    //Representación del grafo mediante la matriz de adyasencia
		private double[][] A;
		Coordenadas Punto = new Coordenadas(0, 0);
		
		// El conjunto de vértices ingresadas por el cliente
		public Grafo(int vertices)
		{
			A = new double[vertices][vertices];
		}
		
		// Creacion de la matriz con las distancias entre puntos
		public void agregarArista(int x, int y, double distancia)
		{
			verificarIndices(x, y);
			A[x][y] = A[y][x] = distancia;
		}
		
		// Se crea un array con las listas de puntos
		public Set<Integer> ListaVertices()
		{
			Set<Integer> ret = new HashSet<Integer>();
			for (int i=1; i<tamano()+1;i++) {
				ret.add(i);
			}
			return ret;
		}
		
		//Devuelve la matriz de ady
		public double [][] MatrizAdy(){
			return this.A;
		}
		
		//Devuelve el valor de la arista ingresada
		public double valorArista(int i, int j){
			return this.A[i][j];
		}
		
		// Cantidad de vertices
		public int tamano()
		{
			return A.length;
		}

		@Override
		public String toString() {
			return "Grafo [ListaVertices()=" + ListaVertices() + "]";
		}

		// Lanza excepciones si los índices no son válidos
		private void verificarIndices(int i, int j)
		{
			verificarVertice(i);
			verificarVertice(j);
			
			if( i == j )
				throw new IllegalArgumentException("No existen aristas entre un vertice y si mismo! vertice = " + i);
		}
		
		// Lanza excepciones si los vertices no son válidos
		private void verificarVertice(int i)
		{
			if( i < 0 || i >= tamano() )
				throw new IllegalArgumentException("El vertice " + i + " no existe!");
		}

}
