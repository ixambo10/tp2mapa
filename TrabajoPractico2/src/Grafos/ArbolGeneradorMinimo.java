package Grafos;

import java.util.ArrayList;

public class ArbolGeneradorMinimo {

	private ArrayList<Integer> verticesBlancos;
	private ArrayList<Integer> verticesNegros;
	private Grafo grafo;
	private double[][] A;
	private double[][] Final;
	private double aux;


	public ArbolGeneradorMinimo(Grafo g) {
		grafo = g;
		A = grafo.MatrizAdy().clone();			      // Se Clona la Matriz de Grafo  
		Final = new double[A.length][A.length];       // Matriz que va a contener las Aristas del AGM
		verticesBlancos = new ArrayList<Integer>();   // Vertices no visitados
		verticesNegros = new ArrayList<Integer>();    // Vertices  visitados

		for (int i = 1; i <= grafo.tamano() - 1; i++) {
			verticesBlancos.add(i); // Carga todos los vertices en la Array verticesBlancos
		}
	}

	//Desarrollo del Arbol generador minimo utilizando el algoritmo de Prim
	public double[][] AlgPrim() {
		verticesNegros.add(0);
		
		while (verticesBlancos.size() != 0) {
			aux = -1;       // Utilizada para la comparacion de la arista minima
			int verticeMasCercaD = 0;
			int verticeMasCercaA = 0;
			
			for (int i = 0; i < verticesNegros.size(); i++) {
				int t = verticesNegros.get(i);
				for (int j = 0; j < verticesBlancos.size(); j++) {
					int k = verticesBlancos.get(j);
					if (t != k) {
						if (aux == -1) { // entra solamente la primera vez
							aux = A[t][k];
							verticeMasCercaD = t;
							verticeMasCercaA = k;
						} else {
							if (A[t][k] < aux) {
								aux = A[t][k];
								verticeMasCercaD = t;
								verticeMasCercaA = k;
							}
						}
					}
				}
			}
			
			// Se guardan las aristas con distancia m�s corta en la matriz Final
			Final[verticeMasCercaD][verticeMasCercaA] = A[verticeMasCercaD][verticeMasCercaA];
			Final[verticeMasCercaA][verticeMasCercaD] = A[verticeMasCercaA][verticeMasCercaD];
			
			//se pinta al vecino visitado
			verticesNegros.add(verticeMasCercaA);
			verticesBlancos.remove(verticesBlancos.indexOf(verticeMasCercaA));
		}

		return Final;
	}
}
